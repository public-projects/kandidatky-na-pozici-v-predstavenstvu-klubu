# _Kandidátky na pozici v představenstvu klubu Pod-O-Lee_

---

První verze kandidátek (_v1_ a _v2_) vytvořeny Štěpánem Tichým (s.tichy@pod.cvut.cz).
Návrhy a vylepšení jsou vítány, pište prosím na admin@pod.cvut.cz.

Ve snaze vylepšení a sjednocení grafického designu kandidátek na funkce v klubu Pod-O-Lee byly vytvořeny dvě nové verze šablon v programu $\LaTeX$. Pro samotné používání $\LaTeX$\, naučení se syntaxe a práce s tímto programem si lze dohledat spoustu tutoriálů ať už v [anglickém] či [českém] jazyce. Pro samotnou kompilaci kódu lze využít:

- Online kompilátor [Overleaf]
- Free software [Texmaker]

## Struktura šablony

Samotná šablona v obou verzích obsahuje 2 důležité soubory v kořenové složce a dva v podsložce `img`.

| Soubor                     | Účel                                          |
| -------------------------- | --------------------------------------------- |
| img/foto.png               | Obrázek, který se objeví na kandidátce        |
| img/podolee-logo_sirka.png | Logo, které se zobrazuje v pravém horním rohu |
| kandidátka.tex             | Hlavní textový soubor šablony                 |
| resume.cls                 | Pomocný soubor s nastaveními                  |

Pro nahrazení obrázku vaší fotkou stačí smazat původni obrázek, nahrát vaši fotku do složky `img` a přepsat výraz `foto.png`. Zkompilováním textového souboru `kandidátka.tex` v programu $\LaTeX$ vznikne PDF soubor stejného názvu. Textový soubor `kandidátka.tex` je složen z textu, který se zobrazí naformátovaný a příkazů, které začínají zpětným lomítkem `\`. Veškerý text za znakem procenta `%` funguje jako komentář, který se v textu nezobrazí.

## Vyplňování šablony

Veškeré potřebné úpravy je třeba dělat pouze v souboru `kandidátka.tex`. V úvodu textového souboru je definována sada zkratek, které se používají později v programu. Uživateli stačí změnit pouze text v závorkách, jako je například v následujícím příkladu funkce správce bloku F.

> \newcommand{\funkce}{správce bloku F}

Ve verzi _v1_ je součástí kandidátky text _O mně_. Jeho znění stačí pouze zkopírovat do textového souboru místo následujícího řádku.

> \lipsum[1-2]

Pro volební program nebo pro bodovou sekci _O mně_ ve verzi _v2_ stačí napsat text místo _bod_ nebo _podbod_ za příkaz `\item` nebo `\subitem`.

> \item \bf{bod} \vspace\*{-1ex}  
> \subitem \normalfont podbod

Datum na konci dokumentu je generováno automaticky dle data systému. Pro zadání pevného datumu stačí nahradit ke konci šablony příkaz `\today`.

---

[![N|Solid](https://su.cvut.cz/user/pages/09.logobanka/05.pod-o-lee/podolee-logo_sirka.svg)](https://pod.cvut.cz/)

[overleaf]: https://www.overleaf.com/
[texmaker]: https://www.xm1math.net/texmaker/
[českém]: http://svat.fjfi.cvut.cz/files/SVAT_1-4-tutorial-zadani.pdf
[anglickém]: https://www.youtube.com/watch?v=0ivLZh9xK1Q&list=PL1D4EAB31D3EBC449
